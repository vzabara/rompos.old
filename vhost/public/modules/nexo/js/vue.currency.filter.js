Vue.filter( 'currency', function (value) {
    if ( tendooOptions.nexo_currency_position === 'before' ) {
        return tendooOptions.nexo_currency + ' ' + parseFloat(value).toFixed(2);
    } else {
        return parseFloat(value).toFixed(2) + ' ' + tendooOptions.nexo_currency;
    }
});