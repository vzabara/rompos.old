NexoPOS - Multipurpose Point Of Sale 
====================================
NexoPOS is a point of sale system cloud based, that fit various businesses from restaurant to grocery stores.

Getting Started
===============
- How to install NexoPOS https://nexopos.com/how-to-download-and-install-nexopos/
- How to install a module 
- How to update NexoPOS : https://www.youtube.com/watch?v=hVkcShuOYTM
- How to create Coupons : https://nexopos.com/documentation/nexopos-tutorials/how-does-coupons-works-on-nexopos/
- How to import items : https://nexopos.com/how-to-import-items-on-nexopos/

Youtube Channel
===============
- Channel : https://www.youtube.com/channel/UCDmcyd-62pnlLf5AuCt6aHA

Contact : 
=========
contact@nexopos.com
