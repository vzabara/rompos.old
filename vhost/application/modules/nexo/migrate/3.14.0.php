<?php
/**
 * Introduce multiple taxes
 * per orders
 * @since 3.14.0
 */
$this->load->model( 'Nexo_Stores' );

$stores         =   $this->Nexo_Stores->get();

array_unshift( $stores, [
    'ID'        =>  0
]);

foreach( $stores as $store ) {
    $store_prefix       =   $store[ 'ID' ] == 0 ? '' : 'store_' . $store[ 'ID' ] . '_';

    $this->db->query( 'CREATE TABLE IF NOT EXISTS `' . $this->db->dbprefix . $store_prefix . 'nexo_commandes_taxes` (
        `ID` int(11) NOT NULL AUTO_INCREMENT,
        `NAME` varchar(200) NOT NULL, 
        `TYPE` varchar(200) NOT NULL,
        `VALUE` float(11) NOT NULL,
        `DATE_CREATION` datetime NOT NULL,
        `AUTHOR` int(11) NOT NULL
        `REF_ORDER` int(11) NOT NULL,
        PRIMARY KEY (`ID`)
    )');

    $columns            =   $this->db->list_fields( $store_prefix . 'nexo_commandes' );
    if( ! in_array( 'TOTAL_TAXES', $columns ) ) {
        $this->db->query( 'ALTER TABLE `' . $this->db->dbprefix . $store_prefix . 'nexo_commandes` 
        ADD `TOTAL_TAXES` float(11) NOT NULL AFTER `REF_SHIPPING_ADDRESS`' );
    }
    
    if ( in_array( 'REF_TAX', $column ) ) {
        $this->db->query( 'ALTER TABLE `' . $this->db->dbprefix . $store_prefix . 'nexo_commandes` 
        DROP COLUMN `REF_TAX`' );
    }
}