-- MySQL dump 10.17  Distrib 10.3.11-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: nexopos
-- ------------------------------------------------------
-- Server version	10.3.11-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tendoo_aauth_groups`
--

DROP TABLE IF EXISTS `tendoo_aauth_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tendoo_aauth_groups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `definition` text DEFAULT NULL,
  `description` text DEFAULT NULL,
  `is_admin` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tendoo_aauth_groups`
--

LOCK TABLES `tendoo_aauth_groups` WRITE;
/*!40000 ALTER TABLE `tendoo_aauth_groups` DISABLE KEYS */;
INSERT INTO `tendoo_aauth_groups` VALUES (4,'master','Master Group','Can create users, install modules, manage options',1),(5,'administrator','Admin Group','Can install modules, manage options',1),(6,'user','User Group','Just a user',1),(7,'store.cashier','Cashier','Role with limited permission for sale',1),(8,'store.manager','Shop Manager','Role with management permissions for the shop.',1),(9,'sub-store.manager','Manager of sub-shop','Role with management permissions of a sub-shop.',1),(10,'store.demo','Role test','Role with permissions to test the features of NexoPOS.',1);
/*!40000 ALTER TABLE `tendoo_aauth_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tendoo_aauth_perm_to_group`
--

DROP TABLE IF EXISTS `tendoo_aauth_perm_to_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tendoo_aauth_perm_to_group` (
  `perm_id` int(11) unsigned NOT NULL,
  `group_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`perm_id`,`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tendoo_aauth_perm_to_group`
--

LOCK TABLES `tendoo_aauth_perm_to_group` WRITE;
/*!40000 ALTER TABLE `tendoo_aauth_perm_to_group` DISABLE KEYS */;
INSERT INTO `tendoo_aauth_perm_to_group` VALUES (1,4),(2,4),(2,5),(3,4),(3,5),(4,4),(4,5),(5,4),(5,5),(6,4),(6,5),(7,4),(7,5),(8,4),(8,5),(9,4),(9,5),(10,4),(10,5),(11,4),(12,4),(13,4),(14,0),(14,4),(14,5),(14,6),(14,8),(14,9),(15,0),(15,4),(15,7),(15,8),(16,0),(16,4),(16,8),(17,0),(17,4),(17,8),(18,0),(18,4),(18,8),(19,0),(19,4),(19,8),(20,0),(20,4),(20,8),(21,0),(21,4),(21,8),(22,0),(22,4),(22,8),(23,0),(23,4),(23,8),(24,0),(24,4),(24,8),(25,0),(25,4),(25,8),(26,0),(26,4),(26,8),(27,0),(27,4),(27,8),(28,0),(28,4),(28,8),(29,0),(29,4),(29,8),(30,0),(30,4),(30,8),(31,0),(31,4),(31,8),(32,0),(32,4),(32,8),(33,0),(33,4),(33,8),(34,0),(34,4),(34,8),(35,0),(35,4),(35,8),(36,0),(36,4),(36,8),(37,0),(37,4),(37,8),(38,0),(38,4),(38,8),(39,0),(39,4),(39,8),(40,0),(40,4),(40,8),(41,0),(41,4),(41,8),(42,0),(42,4),(42,8),(43,0),(43,4),(43,8),(44,0),(44,4),(44,8),(45,0),(45,4),(45,8),(46,0),(46,4),(46,8),(47,0),(47,4),(47,8),(48,0),(48,4),(48,8),(49,0),(49,4),(49,8),(50,0),(50,4),(50,8),(51,0),(51,4),(51,8),(52,0),(52,4),(52,8),(53,0),(53,4),(53,8),(54,0),(54,4),(54,8),(55,0),(55,4),(55,8),(56,0),(56,4),(56,8),(57,0),(57,4),(57,8),(58,0),(58,4),(58,8),(59,7),(60,0),(60,4),(60,8),(60,9),(61,0),(61,4),(61,8),(62,0),(62,4),(62,8),(63,0),(63,4),(63,8),(64,0),(64,4),(64,8),(65,0),(65,4),(65,8),(66,0),(66,4),(66,8),(67,0),(67,4),(67,8),(68,0),(68,4),(68,8),(69,0),(69,4),(69,8),(70,0),(70,4),(70,7),(70,8),(71,0),(71,4),(71,8),(72,0),(72,4),(72,8),(73,0),(73,4),(73,7),(73,8),(73,9),(74,0),(74,4),(74,8),(75,0),(75,4),(75,8),(76,0),(76,4),(76,8),(77,0),(77,4),(77,8),(78,0),(78,4),(78,8),(79,0),(79,4),(79,8),(80,0),(80,4),(80,8),(81,0),(81,4),(81,8),(82,0),(82,4),(82,8),(82,9),(83,0),(83,4),(83,8),(83,9),(84,0),(84,4),(84,8),(84,9),(85,0),(85,4),(85,8),(85,9),(86,0),(86,4),(86,8),(86,9),(87,0),(87,4),(87,8),(87,9),(88,0),(88,4),(88,8),(88,9),(89,0),(89,4),(89,8),(89,9),(90,0),(90,4),(90,8),(90,9),(91,0),(91,4),(91,8),(91,9),(92,0),(92,4),(92,8),(92,9),(93,0),(93,4),(93,8),(93,9);
/*!40000 ALTER TABLE `tendoo_aauth_perm_to_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tendoo_aauth_perm_to_user`
--

DROP TABLE IF EXISTS `tendoo_aauth_perm_to_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tendoo_aauth_perm_to_user` (
  `perm_id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`perm_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tendoo_aauth_perm_to_user`
--

LOCK TABLES `tendoo_aauth_perm_to_user` WRITE;
/*!40000 ALTER TABLE `tendoo_aauth_perm_to_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `tendoo_aauth_perm_to_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tendoo_aauth_perms`
--

DROP TABLE IF EXISTS `tendoo_aauth_perms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tendoo_aauth_perms` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `definition` text DEFAULT NULL,
  `description` text DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=94 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tendoo_aauth_perms`
--

LOCK TABLES `tendoo_aauth_perms` WRITE;
/*!40000 ALTER TABLE `tendoo_aauth_perms` DISABLE KEYS */;
INSERT INTO `tendoo_aauth_perms` VALUES (1,'manage_core','Manage Core','Allow core management'),(2,'create_options','Create Options','Allow option creation'),(3,'edit_options','Edit Options','Allow option edition'),(4,'read_options','Read Options','Allow option read'),(5,'delete_options','Delete Options','Allow option deletion.'),(6,'install_modules','Install Modules','Let user install modules.'),(7,'update_modules','Update Modules','Let user update modules'),(8,'delete_modules','Delete Modules','Let user delete modules'),(9,'toggle_modules','Enable/Disable Modules','Let user enable/disable modules'),(10,'extract_modules','Extract Modules','Let user extract modules'),(11,'create_users','Create Users','Allow create users.'),(12,'edit_users','Edit Users','Allow edit users.'),(13,'delete_users','Delete Users','Allow delete users.'),(14,'edit_profile','Create Options','Allow option creation'),(15,'nexo.create.orders','Create orders',''),(16,'nexo.view.orders','View order list',''),(17,'nexo.edit.orders','Edit orders',''),(18,'nexo.delete.orders','Delete orders',''),(19,'nexo.create.items','Create items',''),(20,'nexo.view.items','See products list',''),(21,'nexo.edit.items','Modifying items',''),(22,'nexo.delete.items','Delete items',''),(23,'nexo.create.categories','Creating categories',''),(24,'nexo.view.categories','See the list of categories',''),(25,'nexo.edit.categories','Edit categories',''),(26,'nexo.delete.categories','Delete Categories',''),(27,'nexo.create.departments','Create departments',''),(28,'nexo.view.departments','See the list of departments',''),(29,'nexo.edit.departments','Modify departments',''),(30,'nexo.delete.departments','Delete departments',''),(31,'nexo.create.providers','Create suppliers',''),(32,'nexo.view.providers','See the list of suppliers',''),(33,'nexo.edit.providers','Edit providers',''),(34,'nexo.delete.providers','Delete providers',''),(35,'nexo.create.supplies','Create supplies',''),(36,'nexo.view.supplies','See the list of supplies',''),(37,'nexo.edit.supplies','Edit supplies',''),(38,'nexo.delete.supplies','Delete supplies',''),(39,'nexo.create.customers-groups','Create Customer Groups',''),(40,'nexo.view.customers-groups','Customers groups lists',''),(41,'nexo.edit.customers-groups','Edit Customer Groups',''),(42,'nexo.delete.customers-groups','Delete Client Groups',''),(43,'nexo.create.customers','Create customers',''),(44,'nexo.view.customers','See the list of customers',''),(45,'nexo.edit.customers','Edit clients',''),(46,'nexo.delete.customers','Delete customers',''),(47,'nexo.create.invoices','Create Invoices',''),(48,'nexo.view.invoices','See the list of invoices',''),(49,'nexo.edit.invoices','Edit Invoices',''),(50,'nexo.delete.invoices','Delete Invoices',''),(51,'nexo.create.taxes','Create taxes',''),(52,'nexo.view.taxes','See the list of taxes',''),(53,'nexo.edit.taxes','Change taxes',''),(54,'nexo.delete.taxes','Remove taxes',''),(55,'nexo.create.registers','Create a cash register',''),(56,'nexo.view.registers','See the list of cash registers',''),(57,'nexo.edit.registers','Modify a cash register',''),(58,'nexo.delete.registers','Delete a cash register',''),(59,'nexo.use.registers','Use a cash register',''),(60,'nexo.view.registers-history','View the history of a caisse',''),(61,'nexo.create.backups','Create & backups',''),(62,'nexo.view.backups','See the list of backups',''),(63,'nexo.edit.backups','Edit backups',''),(64,'nexo.delete.backups','Remove backups',''),(65,'nexo.create.stock-adjustment','Create stock adjustments',''),(66,'nexo.view.stock-adjustment','See the list of adjustments',''),(67,'nexo.edit.stock-adjustment','Edit adjustments',''),(68,'nexo.delete.stock-adjustment','Remove adjustments',''),(69,'nexo.create.stores','Create shops',''),(70,'nexo.view.stores','See the list of shops',''),(71,'nexo.edit.stores','Edit shops',''),(72,'nexo.delete.stores','Remove shops',''),(73,'nexo.enter.stores','Use a shop',''),(74,'nexo.create.coupons','Create coupons',''),(75,'nexo.view.coupons','See the list of coupons',''),(76,'nexo.edit.coupons','Edit Coupons',''),(77,'nexo.delete.coupons','Delete Coupons',''),(78,'nexo.view.refund','Consult a refund',''),(79,'nexo.create.refund','Create a refund',''),(80,'nexo.edit.refund','Edit a refund',''),(81,'nexo.delete.refund','Delete a refund',''),(82,'nexo.read.detailed-report','Read detailed sales',''),(83,'nexo.read.best-sales','Top sellers',''),(84,'nexo.read.daily-sales','Read daily sales',''),(85,'nexo.read.incomes-losses','Incomes and Losses',''),(86,'nexo.read.expenses-listings','Expenses List',''),(87,'nexo.read.cash-flow','Read the cash flow',''),(88,'nexo.read.annual-sales','Read income and losses',''),(89,'nexo.read.cashier-performances','Cashiers performance',''),(90,'nexo.read.customer-statistics','Read customer statistics',''),(91,'nexo.read.inventory-tracking','Read stock tracking',''),(92,'nexo.manage.settings','Option settings',''),(93,'nexo.manage.stores-settings','Stores settings','');
/*!40000 ALTER TABLE `tendoo_aauth_perms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tendoo_aauth_pms`
--

DROP TABLE IF EXISTS `tendoo_aauth_pms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tendoo_aauth_pms` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `sender_id` int(11) unsigned NOT NULL,
  `receiver_id` int(11) unsigned NOT NULL,
  `title` varchar(255) NOT NULL,
  `message` text DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `read` tinyint(1) DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `full_index` (`id`,`sender_id`,`receiver_id`,`read`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tendoo_aauth_pms`
--

LOCK TABLES `tendoo_aauth_pms` WRITE;
/*!40000 ALTER TABLE `tendoo_aauth_pms` DISABLE KEYS */;
/*!40000 ALTER TABLE `tendoo_aauth_pms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tendoo_aauth_system_variables`
--

DROP TABLE IF EXISTS `tendoo_aauth_system_variables`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tendoo_aauth_system_variables` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(100) NOT NULL,
  `value` text DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tendoo_aauth_system_variables`
--

LOCK TABLES `tendoo_aauth_system_variables` WRITE;
/*!40000 ALTER TABLE `tendoo_aauth_system_variables` DISABLE KEYS */;
/*!40000 ALTER TABLE `tendoo_aauth_system_variables` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tendoo_aauth_user_to_group`
--

DROP TABLE IF EXISTS `tendoo_aauth_user_to_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tendoo_aauth_user_to_group` (
  `user_id` int(11) unsigned NOT NULL DEFAULT 0,
  `group_id` int(11) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`user_id`,`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tendoo_aauth_user_to_group`
--

LOCK TABLES `tendoo_aauth_user_to_group` WRITE;
/*!40000 ALTER TABLE `tendoo_aauth_user_to_group` DISABLE KEYS */;
INSERT INTO `tendoo_aauth_user_to_group` VALUES (2,4),(3,4),(4,8);
/*!40000 ALTER TABLE `tendoo_aauth_user_to_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tendoo_aauth_user_variables`
--

DROP TABLE IF EXISTS `tendoo_aauth_user_variables`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tendoo_aauth_user_variables` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `key` varchar(100) NOT NULL,
  `value` text DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tendoo_aauth_user_variables`
--

LOCK TABLES `tendoo_aauth_user_variables` WRITE;
/*!40000 ALTER TABLE `tendoo_aauth_user_variables` DISABLE KEYS */;
/*!40000 ALTER TABLE `tendoo_aauth_user_variables` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tendoo_aauth_users`
--

DROP TABLE IF EXISTS `tendoo_aauth_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tendoo_aauth_users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(100) NOT NULL,
  `pass` varchar(100) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `banned` tinyint(1) DEFAULT 0,
  `last_login` datetime DEFAULT NULL,
  `last_activity` datetime DEFAULT NULL,
  `last_login_attempt` datetime DEFAULT NULL,
  `forgot_exp` text DEFAULT NULL,
  `remember_time` datetime DEFAULT NULL,
  `remember_exp` text DEFAULT NULL,
  `verification_code` text DEFAULT NULL,
  `ip_address` text DEFAULT NULL,
  `login_attempts` int(11) DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tendoo_aauth_users`
--

LOCK TABLES `tendoo_aauth_users` WRITE;
/*!40000 ALTER TABLE `tendoo_aauth_users` DISABLE KEYS */;
INSERT INTO `tendoo_aauth_users` VALUES (2,'example1@example.com','6e8bdd49630e39a823fb2a9dba84ae85951780e45ee44fc264469179ba75cc98','admin',0,'2019-01-18 14:53:09','2019-01-18 14:53:09','2019-01-18 14:00:00',NULL,NULL,NULL,'','127.0.0.1',NULL),(3,'example2@example.com','f9ffd1c372ec529fa59da28d99b9445367e43fa7376991d6df31f114bde1a6b0','manager',0,'2018-12-23 20:00:18','2018-12-23 20:00:18','2018-12-23 20:00:00',NULL,NULL,NULL,'','127.0.0.1',NULL),(4,'example@example.com','72af18ad15cae8127fa7979815b74623626ec70fe67cabb84f6eb67965e32839','shopadmin',0,'2018-12-23 18:17:29','2018-12-23 18:17:29','2018-12-23 18:00:00',NULL,NULL,NULL,'','127.0.0.1',NULL);
/*!40000 ALTER TABLE `tendoo_aauth_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tendoo_nexo_arrivages`
--

DROP TABLE IF EXISTS `tendoo_nexo_arrivages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tendoo_nexo_arrivages` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TITRE` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `DESCRIPTION` text COLLATE utf8_unicode_ci NOT NULL,
  `VALUE` float NOT NULL,
  `ITEMS` int(11) NOT NULL,
  `REF_PROVIDER` int(11) NOT NULL,
  `DATE_CREATION` datetime NOT NULL,
  `DATE_MOD` datetime NOT NULL,
  `AUTHOR` int(11) NOT NULL,
  `FOURNISSEUR_REF_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tendoo_nexo_arrivages`
--

LOCK TABLES `tendoo_nexo_arrivages` WRITE;
/*!40000 ALTER TABLE `tendoo_nexo_arrivages` DISABLE KEYS */;
INSERT INTO `tendoo_nexo_arrivages` VALUES (1,'Charlottes Russian 2016','special collection of clothing Charlotte Russe',0,0,0,'2018-12-27 12:17:37','0000-00-00 00:00:00',2,1),(2,'Charlottes Russian 2017','special collection of clothing Charlotte Russe',0,0,0,'2018-12-27 12:17:37','0000-00-00 00:00:00',2,2);
/*!40000 ALTER TABLE `tendoo_nexo_arrivages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tendoo_nexo_articles`
--

DROP TABLE IF EXISTS `tendoo_nexo_articles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tendoo_nexo_articles` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DESIGN` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `ALTERNATIVE_NAME` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `REF_RAYON` int(11) NOT NULL,
  `REF_SHIPPING` int(11) NOT NULL,
  `REF_CATEGORIE` int(11) NOT NULL,
  `REF_PROVIDER` int(11) NOT NULL,
  `REF_TAXE` int(11) NOT NULL,
  `TAX_TYPE` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `QUANTITY` int(11) NOT NULL,
  `SKU` varchar(220) COLLATE utf8_unicode_ci NOT NULL,
  `QUANTITE_RESTANTE` int(11) NOT NULL,
  `QUANTITE_VENDU` int(11) NOT NULL,
  `DEFECTUEUX` int(11) NOT NULL,
  `PRIX_DACHAT` float NOT NULL,
  `FRAIS_ACCESSOIRE` float NOT NULL,
  `COUT_DACHAT` float NOT NULL,
  `TAUX_DE_MARGE` float NOT NULL,
  `PRIX_DE_VENTE` float NOT NULL,
  `PRIX_DE_VENTE_TTC` float NOT NULL,
  `SHADOW_PRICE` float NOT NULL,
  `TAILLE` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `POIDS` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `COULEUR` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `HAUTEUR` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `LARGEUR` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `PRIX_PROMOTIONEL` float NOT NULL,
  `SPECIAL_PRICE_START_DATE` datetime NOT NULL,
  `SPECIAL_PRICE_END_DATE` datetime NOT NULL,
  `DESCRIPTION` text COLLATE utf8_unicode_ci NOT NULL,
  `APERCU` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `CODEBAR` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `DATE_CREATION` datetime NOT NULL,
  `DATE_MOD` datetime NOT NULL,
  `AUTHOR` int(11) NOT NULL,
  `TYPE` int(11) NOT NULL,
  `STATUS` int(11) NOT NULL,
  `STOCK_ENABLED` int(11) NOT NULL,
  `STOCK_ALERT` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `ALERT_QUANTITY` int(11) NOT NULL,
  `EXPIRATION_DATE` datetime NOT NULL,
  `ON_EXPIRE_ACTION` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `AUTO_BARCODE` int(11) NOT NULL,
  `BARCODE_TYPE` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `USE_VARIATION` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SKU` (`SKU`),
  UNIQUE KEY `CODEBAR` (`CODEBAR`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tendoo_nexo_articles`
--

LOCK TABLES `tendoo_nexo_articles` WRITE;
/*!40000 ALTER TABLE `tendoo_nexo_articles` DISABLE KEYS */;
INSERT INTO `tendoo_nexo_articles` VALUES (1,'Velvet V-Neck Body Suit','',0,1,5,0,0,'',80550,'BODYSUIT01',80550,0,0,10,2,12,79.9,17.99,17.99,19,'38','0','Black','25','8',0,'0000-00-00 00:00:00','0000-00-00 00:00:00','','../../modules/nexo/images/charlotterusse/velvetvneckbodysuit.jpg','0000001','2018-12-27 12:17:37','0000-00-00 00:00:00',2,1,1,1,'',0,'0000-00-00 00:00:00','',0,'',0),(2,'Qupid Lucite Heel Dress Booties','',0,1,3,0,0,'',80550,'BOOTIES01',80550,0,0,30,2,32,109.9,42.99,42.99,43,'38','0','Black','25','8',0,'0000-00-00 00:00:00','0000-00-00 00:00:00','','../../modules/nexo/images/charlotterusse/quidluciteheeldressbooties.jpg','0000002','2018-12-27 12:17:37','0000-00-00 00:00:00',2,1,1,1,'',0,'0000-00-00 00:00:00','',0,'',0),(3,'Twisted High-Low Top','',0,1,5,0,0,'',6000,'BODYSUIT02',6000,0,0,15,2,17,19.9,18.99,18.99,19,'38','0','Black','25','8',0,'0000-00-00 00:00:00','0000-00-00 00:00:00','','../../modules/nexo/images/charlotterusse/twistedhighlowtop.jpg','0000003','2018-12-27 12:17:37','0000-00-00 00:00:00',2,1,1,1,'',0,'0000-00-00 00:00:00','',0,'',0),(4,'Notched Floral Off-The Shoulder','',0,1,5,0,0,'',6000,'BODYSUIT03',6000,0,0,15,2,21,29.9,19.99,19.99,19,'38','0','Flored','25','8',0,'0000-00-00 00:00:00','0000-00-00 00:00:00','','../../modules/nexo/images/charlotterusse/floralnotchedofftheshoulder.jpg','0000004','2018-12-27 12:17:37','0000-00-00 00:00:00',2,1,1,1,'',0,'0000-00-00 00:00:00','',0,'',0),(5,'Sequin Cropped Bomber Jacket','',0,1,5,0,0,'',6000,'BODYSUIT04',6000,0,0,30,2,51,109.9,42.99,42.99,19,'38','0','Flored','25','8',0,'0000-00-00 00:00:00','0000-00-00 00:00:00','','../../modules/nexo/images/charlotterusse/sequincroppedbomberjacket.jpg','0000005','2018-12-27 12:17:37','0000-00-00 00:00:00',2,1,1,1,'',0,'0000-00-00 00:00:00','',0,'',0),(6,'Sequin Cropped Bomber Jacket','',0,1,6,0,0,'',6000,'LEGGINGS01',6000,0,0,10,2,12,29.9,14.99,14.99,16,'38','0','Pink','25','8',0,'0000-00-00 00:00:00','0000-00-00 00:00:00','','../../modules/nexo/images/charlotterusse/pushuplowriseleggings.jpg','0000006','2018-12-27 12:17:37','0000-00-00 00:00:00',2,1,1,1,'',0,'0000-00-00 00:00:00','',0,'',0),(7,'Varsity Stripe Jogger Pants','',0,1,6,0,0,'',6000,'LEGGINGS02',6000,0,0,10,2,12,79.9,19.99,19.99,16,'38','0','Black','25','8',0,'0000-00-00 00:00:00','0000-00-00 00:00:00','','../../modules/nexo/images/charlotterusse/varsitystripejoggerpants.jpg','0000007','2018-12-27 12:17:37','0000-00-00 00:00:00',2,1,1,1,'',0,'0000-00-00 00:00:00','',0,'',0),(8,'Ribbed Lace Up BodySuit','',0,1,5,0,0,'',6000,'BODYSUIT05',6000,0,0,10,2,12,59.9,17.99,17.99,16,'38','0','Pink','25','8',0,'0000-00-00 00:00:00','0000-00-00 00:00:00','','../../modules/nexo/images/charlotterusse/ribberlaceupbody.jpg','68478945','2018-12-27 12:17:37','0000-00-00 00:00:00',2,1,1,1,'',0,'0000-00-00 00:00:00','',0,'',0),(9,'Marled French Terry Jogger Pants','',0,1,5,0,0,'',6000,'LEGGINGS03',6000,0,0,10,2,12,69.9,18.99,18.99,16,'38','0','Pink','25','8',0,'0000-00-00 00:00:00','0000-00-00 00:00:00','','../../modules/nexo/images/charlotterusse/marledfrenchterryjoggerpants.jpg','0000009','2018-12-27 12:17:37','0000-00-00 00:00:00',2,1,1,1,'',0,'0000-00-00 00:00:00','',0,'',0),(10,'Floating Mock Neck Strapless Dress','',0,1,4,0,0,'',6000,'DRESS03',6000,0,0,10,2,12,179.9,29.99,29.99,16,'38','0','Pink','25','8',0,'0000-00-00 00:00:00','0000-00-00 00:00:00','','../../modules/nexo/images/charlotterusse/floatingmocknectstraplessdress.jpg','0000010','2018-12-27 12:17:37','0000-00-00 00:00:00',2,1,1,1,'',0,'0000-00-00 00:00:00','',0,'',0),(11,'Shimmer Open Back Bodycon Dress','',0,1,4,0,0,'',6000,'DRESS02',6000,0,0,10,2,12,179.9,29.99,29.99,16,'38','0','Pink','25','8',0,'0000-00-00 00:00:00','0000-00-00 00:00:00','','../../modules/nexo/images/charlotterusse/shimmeropenbackbodycondress.jpg','0000011','2018-12-27 12:17:37','0000-00-00 00:00:00',2,1,1,1,'',0,'0000-00-00 00:00:00','',0,'',0),(12,'Chrochet Trim Cold Should Sweatshirt','',0,1,7,0,0,'',6000,'SWEATSHIRT01',6000,0,0,18,2,20,11.0556,21.99,21.99,16,'38','0','Pink','25','8',0,'0000-00-00 00:00:00','0000-00-00 00:00:00','','../../modules/nexo/images/charlotterusse/crochettrimcoldshoulder.jpg','0000012','2018-12-27 12:17:37','0000-00-00 00:00:00',2,1,1,1,'',0,'0000-00-00 00:00:00','',0,'',0),(13,'Mirrored Sequin V-Neck Romper','',0,1,5,0,0,'',6000,'BODYSUIT07',6000,0,0,30,2,32,16.6333,36.99,36.99,16,'38','0','Starry','25','8',0,'0000-00-00 00:00:00','0000-00-00 00:00:00','','../../modules/nexo/images/charlotterusse/mirrorsequinvneck.jpg','0000013','2018-12-27 12:17:37','0000-00-00 00:00:00',2,1,1,1,'',0,'0000-00-00 00:00:00','',0,'',0),(14,'Velvet Mock Nect Cut-Out Jumpshit','',0,1,5,0,0,'',6000,'BODYSUIT08',6000,0,0,30,2,32,9.96667,34.99,34.99,16,'38','0','Dark blue','25','8',0,'0000-00-00 00:00:00','0000-00-00 00:00:00','','../../modules/nexo/images/charlotterusse/velvetmocknectcutoutjumpsuit.jpg','0000014','2018-12-27 12:17:37','0000-00-00 00:00:00',2,1,1,1,'',0,'0000-00-00 00:00:00','',0,'',0),(15,'Lace & Leather Choker Necklaces Faux - 3 Pack','',0,1,2,0,0,'',6000,'ACCESSOIRE1',6000,0,0,5,2,32,-0.0333333,6.99,6.99,7,'38','0','','25','8',0,'0000-00-00 00:00:00','0000-00-00 00:00:00','','../../modules/nexo/images/charlotterusse/lacefauxleatherchokernecklaces.jpg','0000015','2018-12-27 12:17:37','0000-00-00 00:00:00',2,1,1,1,'',0,'0000-00-00 00:00:00','',0,'',0),(16,'Hacci Cold Shoulder Raglan Tee','',0,1,7,0,0,'',6000,'SWEATSHIRT02',6000,0,0,5,2,32,36.6333,17.99,17.99,7,'38','0','','25','8',0,'0000-00-00 00:00:00','0000-00-00 00:00:00','','../../modules/nexo/images/charlotterusse/haccicoldshoulderreglantee.jpg','0000016','2018-12-27 12:17:37','0000-00-00 00:00:00',2,1,1,1,'',0,'0000-00-00 00:00:00','',0,'',0),(17,'Marled Caged Strappy bodysuit','',0,1,7,0,0,'',6000,'BODYSUIT09',6000,0,0,5,2,32,39.9667,18.99,18.99,7,'38','0','','25','8',0,'0000-00-00 00:00:00','0000-00-00 00:00:00','','../../modules/nexo/images/charlotterusse/marledstrappycaedbodysuit.jpg','0000017','2018-12-27 12:17:37','0000-00-00 00:00:00',2,1,1,1,'',0,'0000-00-00 00:00:00','',0,'',0),(18,'Embroidered Mesh Mock Neck Crop Top','',0,1,8,0,0,'',6000,'CROPTOP01',6000,0,0,5,2,32,36.6333,17.99,17.99,7,'38','0','','25','8',0,'0000-00-00 00:00:00','0000-00-00 00:00:00','','../../modules/nexo/images/charlotterusse/embroideredmeshmocknectcrop.jpg','0000018','2018-12-27 12:17:37','0000-00-00 00:00:00',2,1,1,1,'',0,'0000-00-00 00:00:00','',0,'',0),(19,'Qupid Metal Trim Platform Pumps','',0,1,8,0,0,'',6000,'SHOES01',6000,0,0,5,2,32,96.6333,35.99,35.99,7,'38','0','','25','8',0,'0000-00-00 00:00:00','0000-00-00 00:00:00','','../../modules/nexo/images/charlotterusse/qupidmetatrimplateformpumps.jpg','0000019','2018-12-27 12:17:37','0000-00-00 00:00:00',2,1,1,1,'',0,'0000-00-00 00:00:00','',0,'',0),(20,'Floating Floral Mock Neck Top','',0,1,8,0,0,'',6000,'CROPTOP02',6000,0,0,5,2,32,56.6333,23.99,23.99,7,'38','0','','25','8',0,'0000-00-00 00:00:00','0000-00-00 00:00:00','','../../modules/nexo/images/charlotterusse/floralfloatingmocknecktop.jpg','0000020','2018-12-27 12:17:37','0000-00-00 00:00:00',2,1,1,1,'',0,'0000-00-00 00:00:00','',0,'',0);
/*!40000 ALTER TABLE `tendoo_nexo_articles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tendoo_nexo_articles_meta`
--

DROP TABLE IF EXISTS `tendoo_nexo_articles_meta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tendoo_nexo_articles_meta` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `REF_ARTICLE` int(11) NOT NULL,
  `KEY` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `VALUE` text COLLATE utf8_unicode_ci NOT NULL,
  `DATE_CREATION` datetime NOT NULL,
  `DATE_MOD` datetime NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tendoo_nexo_articles_meta`
--

LOCK TABLES `tendoo_nexo_articles_meta` WRITE;
/*!40000 ALTER TABLE `tendoo_nexo_articles_meta` DISABLE KEYS */;
/*!40000 ALTER TABLE `tendoo_nexo_articles_meta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tendoo_nexo_articles_stock_flow`
--

DROP TABLE IF EXISTS `tendoo_nexo_articles_stock_flow`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tendoo_nexo_articles_stock_flow` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `REF_ARTICLE_BARCODE` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `BEFORE_QUANTITE` int(11) NOT NULL,
  `QUANTITE` int(11) NOT NULL,
  `AFTER_QUANTITE` int(11) NOT NULL,
  `DATE_CREATION` datetime NOT NULL,
  `AUTHOR` int(11) NOT NULL,
  `REF_COMMAND_CODE` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `REF_SHIPPING` int(11) NOT NULL,
  `TYPE` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `UNIT_PRICE` float NOT NULL,
  `TOTAL_PRICE` float NOT NULL,
  `REF_PROVIDER` int(11) NOT NULL,
  `DESCRIPTION` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tendoo_nexo_articles_stock_flow`
--

LOCK TABLES `tendoo_nexo_articles_stock_flow` WRITE;
/*!40000 ALTER TABLE `tendoo_nexo_articles_stock_flow` DISABLE KEYS */;
INSERT INTO `tendoo_nexo_articles_stock_flow` VALUES (1,'0000001',80550,1,80549,'2018-12-27 13:19:26',2,'5KI52A',0,'sale',17.99,17.99,0,''),(2,'0000001',80550,1,80549,'2018-12-27 13:19:31',2,'5KI52A',0,'sale',17.99,17.99,0,''),(3,'0000001',80549,1,80548,'2018-12-27 14:02:25',2,'DP8H9E',0,'sale',17.99,17.99,0,''),(4,'0000001',80549,1,80548,'2018-12-27 14:02:30',2,'DP8H9E',0,'sale',17.99,17.99,0,''),(5,'0000001',80548,1,80547,'2018-12-27 16:09:21',2,'GFGQVL',0,'sale',17.99,17.99,0,''),(6,'0000003',6000,1,5999,'2018-12-27 16:11:04',2,'VTKPU3',0,'sale',18.99,18.99,0,''),(7,'0000003',6000,1,5999,'2018-12-27 16:11:10',2,'VTKPU3',0,'sale',18.99,18.99,0,''),(8,'0000001',80547,1,80546,'2018-12-27 16:16:25',2,'AAXD5Z',0,'sale',17.99,17.99,0,''),(9,'0000001',80546,1,80545,'2018-12-27 16:18:12',2,'KNNUK5',0,'sale',17.99,17.99,0,''),(10,'0000001',80545,1,80544,'2018-12-27 16:19:54',2,'FT6OOL',0,'sale',17.99,17.99,0,''),(11,'0000001',80544,1,80543,'2018-12-27 16:22:56',2,'13UYAC',0,'sale',17.99,17.99,0,''),(12,'0000001',80543,1,80542,'2018-12-27 16:26:18',2,'N2Q8I3',0,'sale',17.99,17.99,0,''),(13,'0000001',80542,1,80541,'2018-12-27 16:27:26',2,'94JWMI',0,'sale',17.99,17.99,0,''),(14,'0000001',80541,1,80540,'2018-12-27 16:27:54',2,'TI6VZW',0,'sale',17.99,17.99,0,''),(15,'0000001',80540,1,80539,'2018-12-27 16:30:22',2,'IAN7ZU',0,'sale',17.99,17.99,0,''),(16,'0000001',80539,1,80538,'2018-12-27 16:33:49',2,'FQA7X3',0,'sale',17.99,17.99,0,''),(17,'0000001',80538,1,80537,'2018-12-27 16:36:33',2,'YSXQBO',0,'sale',17.99,17.99,0,''),(18,'0000001',80548,1,80547,'2019-01-18 14:45:01',2,'YAX074',0,'sale',17.99,17.99,0,''),(19,'0000001',80547,1,80546,'2019-01-18 14:45:31',2,'AS7J0F',0,'sale',17.99,17.99,0,'');
/*!40000 ALTER TABLE `tendoo_nexo_articles_stock_flow` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tendoo_nexo_articles_variations`
--

DROP TABLE IF EXISTS `tendoo_nexo_articles_variations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tendoo_nexo_articles_variations` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `REF_ARTICLE` int(11) NOT NULL,
  `VAR_DESIGN` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `VAR_DESCRIPTION` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `VAR_PRIX_DE_VENTE` float NOT NULL,
  `VAR_QUANTITE_TOTALE` int(11) NOT NULL,
  `VAR_QUANTITE_RESTANTE` int(11) NOT NULL,
  `VAR_QUANTITE_VENDUE` int(11) NOT NULL,
  `VAR_COULEUR` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `VAR_TAILLE` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `VAR_POIDS` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `VAR_HAUTEUR` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `VAR_LARGEUR` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `VAR_SHADOW_PRICE` float NOT NULL,
  `VAR_SPECIAL_PRICE_START_DATE` datetime NOT NULL,
  `VAR_SPECIAL_PRICE_END_DATE` datetime NOT NULL,
  `VAR_APERCU` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tendoo_nexo_articles_variations`
--

LOCK TABLES `tendoo_nexo_articles_variations` WRITE;
/*!40000 ALTER TABLE `tendoo_nexo_articles_variations` DISABLE KEYS */;
/*!40000 ALTER TABLE `tendoo_nexo_articles_variations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tendoo_nexo_categories`
--

DROP TABLE IF EXISTS `tendoo_nexo_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tendoo_nexo_categories` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NOM` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `DESCRIPTION` text COLLATE utf8_unicode_ci NOT NULL,
  `DATE_CREATION` datetime NOT NULL,
  `DATE_MOD` datetime NOT NULL,
  `AUTHOR` int(11) NOT NULL,
  `PARENT_REF_ID` int(11) NOT NULL,
  `THUMB` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tendoo_nexo_categories`
--

LOCK TABLES `tendoo_nexo_categories` WRITE;
/*!40000 ALTER TABLE `tendoo_nexo_categories` DISABLE KEYS */;
INSERT INTO `tendoo_nexo_categories` VALUES (1,'Womens','Category Women','2018-12-27 12:17:37','0000-00-00 00:00:00',2,0,''),(2,'Accessories','Accessories category','2018-12-27 12:17:37','0000-00-00 00:00:00',2,1,''),(3,'Shoes','Shoes Category','2018-12-27 12:17:37','0000-00-00 00:00:00',2,1,''),(4,'Dresses','Dresses Category','2018-12-27 12:17:37','0000-00-00 00:00:00',2,1,''),(5,'Set','Sets Category','2018-12-27 12:17:37','0000-00-00 00:00:00',2,1,''),(6,'Leggings','Category Leggings','2018-12-27 12:17:37','0000-00-00 00:00:00',2,1,''),(7,'Sweat Shirt','Category Sweat Shirt','2018-12-27 12:17:37','0000-00-00 00:00:00',2,1,''),(8,'Crop Top','Category Crop Top','2018-12-27 12:17:37','0000-00-00 00:00:00',2,1,'');
/*!40000 ALTER TABLE `tendoo_nexo_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tendoo_nexo_clients`
--

DROP TABLE IF EXISTS `tendoo_nexo_clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tendoo_nexo_clients` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NOM` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `PRENOM` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `POIDS` int(11) NOT NULL,
  `TEL` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `EMAIL` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `DESCRIPTION` text COLLATE utf8_unicode_ci NOT NULL,
  `DATE_NAISSANCE` datetime NOT NULL,
  `ADRESSE` text COLLATE utf8_unicode_ci NOT NULL,
  `NBR_COMMANDES` int(11) NOT NULL,
  `OVERALL_COMMANDES` int(11) NOT NULL,
  `DISCOUNT_ACTIVE` int(11) NOT NULL,
  `TOTAL_SPEND` float NOT NULL,
  `LAST_ORDER` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `AVATAR` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `STATE` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `CITY` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `POST_CODE` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `COUNTRY` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `COMPANY_NAME` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `DATE_CREATION` datetime NOT NULL,
  `DATE_MOD` datetime NOT NULL,
  `REF_GROUP` int(11) NOT NULL,
  `AUTHOR` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tendoo_nexo_clients`
--

LOCK TABLES `tendoo_nexo_clients` WRITE;
/*!40000 ALTER TABLE `tendoo_nexo_clients` DISABLE KEYS */;
INSERT INTO `tendoo_nexo_clients` VALUES (1,'Customer Account','',0,'0','user@tendoo.org','','0000-00-00 00:00:00','',0,0,0,0,'','','','','','','','0000-00-00 00:00:00','0000-00-00 00:00:00',0,0),(2,'John Doe','',0,'0','johndoe@tendoo.org','','0000-00-00 00:00:00','',0,0,0,0,'','','','','','','','0000-00-00 00:00:00','0000-00-00 00:00:00',0,0),(3,'Jane Doe','',0,'0','janedoe@tendoo.org','','0000-00-00 00:00:00','',0,0,0,0,'','','','','','','','0000-00-00 00:00:00','0000-00-00 00:00:00',0,0),(4,'Blair Jersyer','',0,'0','carlosjohnsonluv2004@gmail.com','','0000-00-00 00:00:00','',0,0,0,0,'','','','','','','','0000-00-00 00:00:00','0000-00-00 00:00:00',0,0);
/*!40000 ALTER TABLE `tendoo_nexo_clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tendoo_nexo_clients_address`
--

DROP TABLE IF EXISTS `tendoo_nexo_clients_address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tendoo_nexo_clients_address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `surname` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `enterprise` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `address_1` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `address_2` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `pobox` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `ref_client` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tendoo_nexo_clients_address`
--

LOCK TABLES `tendoo_nexo_clients_address` WRITE;
/*!40000 ALTER TABLE `tendoo_nexo_clients_address` DISABLE KEYS */;
/*!40000 ALTER TABLE `tendoo_nexo_clients_address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tendoo_nexo_clients_groups`
--

DROP TABLE IF EXISTS `tendoo_nexo_clients_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tendoo_nexo_clients_groups` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `DESCRIPTION` text COLLATE utf8_unicode_ci NOT NULL,
  `DATE_CREATION` datetime NOT NULL,
  `DATE_MODIFICATION` datetime NOT NULL,
  `DISCOUNT_TYPE` varchar(220) COLLATE utf8_unicode_ci NOT NULL,
  `DISCOUNT_PERCENT` float NOT NULL,
  `DISCOUNT_AMOUNT` float NOT NULL,
  `DISCOUNT_ENABLE_SCHEDULE` varchar(220) COLLATE utf8_unicode_ci NOT NULL,
  `DISCOUNT_START` datetime NOT NULL,
  `DISCOUNT_END` datetime NOT NULL,
  `AUTHOR` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tendoo_nexo_clients_groups`
--

LOCK TABLES `tendoo_nexo_clients_groups` WRITE;
/*!40000 ALTER TABLE `tendoo_nexo_clients_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `tendoo_nexo_clients_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tendoo_nexo_clients_meta`
--

DROP TABLE IF EXISTS `tendoo_nexo_clients_meta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tendoo_nexo_clients_meta` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `KEY` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `VALUE` text COLLATE utf8_unicode_ci NOT NULL,
  `REF_CLIENT` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tendoo_nexo_clients_meta`
--

LOCK TABLES `tendoo_nexo_clients_meta` WRITE;
/*!40000 ALTER TABLE `tendoo_nexo_clients_meta` DISABLE KEYS */;
/*!40000 ALTER TABLE `tendoo_nexo_clients_meta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tendoo_nexo_commandes`
--

DROP TABLE IF EXISTS `tendoo_nexo_commandes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tendoo_nexo_commandes` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TITRE` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `DESCRIPTION` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `CODE` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `REF_CLIENT` int(11) NOT NULL,
  `REF_REGISTER` int(11) NOT NULL,
  `TYPE` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `DATE_CREATION` datetime NOT NULL,
  `DATE_MOD` datetime NOT NULL,
  `PAYMENT_TYPE` varchar(220) COLLATE utf8_unicode_ci NOT NULL,
  `AUTHOR` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `SOMME_PERCU` float NOT NULL,
  `REMISE` float NOT NULL,
  `RABAIS` float NOT NULL,
  `RISTOURNE` float NOT NULL,
  `REMISE_TYPE` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `REMISE_PERCENT` float NOT NULL,
  `RABAIS_PERCENT` float NOT NULL,
  `RISTOURNE_PERCENT` float NOT NULL,
  `TOTAL` float NOT NULL,
  `DISCOUNT_TYPE` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `TVA` float NOT NULL,
  `GROUP_DISCOUNT` float DEFAULT NULL,
  `REF_SHIPPING_ADDRESS` int(11) NOT NULL,
  `TOTAL_TAXES` float NOT NULL,
  `REF_TAX` int(11) NOT NULL,
  `SHIPPING_AMOUNT` float NOT NULL,
  `STATUS` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `EXPIRATION_DATE` datetime NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `CODE` (`CODE`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tendoo_nexo_commandes`
--

LOCK TABLES `tendoo_nexo_commandes` WRITE;
/*!40000 ALTER TABLE `tendoo_nexo_commandes` DISABLE KEYS */;
/*!40000 ALTER TABLE `tendoo_nexo_commandes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tendoo_nexo_commandes_coupons`
--

DROP TABLE IF EXISTS `tendoo_nexo_commandes_coupons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tendoo_nexo_commandes_coupons` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `REF_COMMAND` int(11) NOT NULL,
  `REF_COUPON` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tendoo_nexo_commandes_coupons`
--

LOCK TABLES `tendoo_nexo_commandes_coupons` WRITE;
/*!40000 ALTER TABLE `tendoo_nexo_commandes_coupons` DISABLE KEYS */;
/*!40000 ALTER TABLE `tendoo_nexo_commandes_coupons` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tendoo_nexo_commandes_meta`
--

DROP TABLE IF EXISTS `tendoo_nexo_commandes_meta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tendoo_nexo_commandes_meta` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `REF_ORDER_ID` int(11) NOT NULL,
  `KEY` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `VALUE` text COLLATE utf8_unicode_ci NOT NULL,
  `DATE_CREATION` datetime NOT NULL,
  `DATE_MOD` datetime NOT NULL,
  `AUTHOR` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tendoo_nexo_commandes_meta`
--

LOCK TABLES `tendoo_nexo_commandes_meta` WRITE;
/*!40000 ALTER TABLE `tendoo_nexo_commandes_meta` DISABLE KEYS */;
/*!40000 ALTER TABLE `tendoo_nexo_commandes_meta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tendoo_nexo_commandes_paiements`
--

DROP TABLE IF EXISTS `tendoo_nexo_commandes_paiements`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tendoo_nexo_commandes_paiements` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `REF_COMMAND_CODE` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `MONTANT` float NOT NULL,
  `AUTHOR` int(11) NOT NULL,
  `DATE_CREATION` datetime NOT NULL,
  `PAYMENT_TYPE` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `OPERATION` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tendoo_nexo_commandes_paiements`
--

LOCK TABLES `tendoo_nexo_commandes_paiements` WRITE;
/*!40000 ALTER TABLE `tendoo_nexo_commandes_paiements` DISABLE KEYS */;
/*!40000 ALTER TABLE `tendoo_nexo_commandes_paiements` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tendoo_nexo_commandes_produits`
--

DROP TABLE IF EXISTS `tendoo_nexo_commandes_produits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tendoo_nexo_commandes_produits` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `REF_PRODUCT_CODEBAR` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `REF_COMMAND_CODE` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `QUANTITE` int(11) NOT NULL,
  `PRIX` float NOT NULL,
  `PRIX_BRUT` float NOT NULL,
  `PRIX_TOTAL` float NOT NULL,
  `PRIX_BRUT_TOTAL` float NOT NULL,
  `DISCOUNT_TYPE` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `DISCOUNT_AMOUNT` float NOT NULL,
  `DISCOUNT_PERCENT` float NOT NULL,
  `NAME` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `ALTERNATIVE_NAME` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `DESCRIPTION` text COLLATE utf8_unicode_ci NOT NULL,
  `INLINE` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tendoo_nexo_commandes_produits`
--

LOCK TABLES `tendoo_nexo_commandes_produits` WRITE;
/*!40000 ALTER TABLE `tendoo_nexo_commandes_produits` DISABLE KEYS */;
/*!40000 ALTER TABLE `tendoo_nexo_commandes_produits` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tendoo_nexo_commandes_produits_meta`
--

DROP TABLE IF EXISTS `tendoo_nexo_commandes_produits_meta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tendoo_nexo_commandes_produits_meta` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `REF_COMMAND_PRODUCT` int(11) NOT NULL,
  `REF_COMMAND_CODE` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `KEY` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `VALUE` text COLLATE utf8_unicode_ci NOT NULL,
  `DATE_CREATION` datetime NOT NULL,
  `DATE_MODIFICATION` datetime NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tendoo_nexo_commandes_produits_meta`
--

LOCK TABLES `tendoo_nexo_commandes_produits_meta` WRITE;
/*!40000 ALTER TABLE `tendoo_nexo_commandes_produits_meta` DISABLE KEYS */;
/*!40000 ALTER TABLE `tendoo_nexo_commandes_produits_meta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tendoo_nexo_commandes_refunds`
--

DROP TABLE IF EXISTS `tendoo_nexo_commandes_refunds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tendoo_nexo_commandes_refunds` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TITLE` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `SUB_TOTAL` float NOT NULL,
  `TOTAL` float NOT NULL,
  `SHIPPING` float NOT NULL,
  `PAYMENT_TYPE` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `DESCRIPTION` text COLLATE utf8_unicode_ci NOT NULL,
  `DATE_CREATION` datetime NOT NULL,
  `AUTHOR` int(11) NOT NULL,
  `REF_ORDER` int(11) NOT NULL,
  `TYPE` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tendoo_nexo_commandes_refunds`
--

LOCK TABLES `tendoo_nexo_commandes_refunds` WRITE;
/*!40000 ALTER TABLE `tendoo_nexo_commandes_refunds` DISABLE KEYS */;
/*!40000 ALTER TABLE `tendoo_nexo_commandes_refunds` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tendoo_nexo_commandes_refunds_products`
--

DROP TABLE IF EXISTS `tendoo_nexo_commandes_refunds_products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tendoo_nexo_commandes_refunds_products` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `REF_ITEM` int(11) NOT NULL,
  `NAME` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `REF_REFUND` int(11) NOT NULL,
  `PRICE` float NOT NULL,
  `QUANTITY` float NOT NULL,
  `TOTAL_PRICE` float NOT NULL,
  `STATUS` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `DESCRIPTION` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `DATE_CREATION` datetime NOT NULL,
  `DATE_MOD` datetime NOT NULL,
  `AUTHOR` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tendoo_nexo_commandes_refunds_products`
--

LOCK TABLES `tendoo_nexo_commandes_refunds_products` WRITE;
/*!40000 ALTER TABLE `tendoo_nexo_commandes_refunds_products` DISABLE KEYS */;
/*!40000 ALTER TABLE `tendoo_nexo_commandes_refunds_products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tendoo_nexo_commandes_shippings`
--

DROP TABLE IF EXISTS `tendoo_nexo_commandes_shippings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tendoo_nexo_commandes_shippings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ref_shipping` int(11) NOT NULL,
  `ref_order` int(11) NOT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `surname` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `address_1` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `address_2` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `pobox` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `enterprise` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `price` float NOT NULL,
  `email` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tendoo_nexo_commandes_shippings`
--

LOCK TABLES `tendoo_nexo_commandes_shippings` WRITE;
/*!40000 ALTER TABLE `tendoo_nexo_commandes_shippings` DISABLE KEYS */;
INSERT INTO `tendoo_nexo_commandes_shippings` VALUES (1,0,1,'','','','','','','','','','',0,'',''),(2,0,2,'','','','','','','','','','',0,'',''),(3,0,3,'','','','','','','','','','',0,'',''),(4,0,4,'','','','','','','','','','',0,'',''),(5,0,5,'','','','','','','','','','',0,'',''),(6,0,6,'','','','','','','','','','',0,'',''),(7,0,7,'','','','','','','','','','',0,'',''),(8,0,8,'','','','','','','','','','',0,'',''),(9,0,9,'','','','','','','','','','',0,'',''),(10,0,10,'','','','','','','','','','',0,'',''),(11,0,11,'','','','','','','','','','',0,'',''),(12,0,12,'','','','','','','','','','',0,'',''),(13,0,13,'','','','','','','','','','',0,'',''),(14,0,14,'','','','','','','','','','',0,'',''),(15,0,15,'','','','','','','','','','',0,'',''),(16,0,16,'','','','','','','','','','',0,'','');
/*!40000 ALTER TABLE `tendoo_nexo_commandes_shippings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tendoo_nexo_coupons`
--

DROP TABLE IF EXISTS `tendoo_nexo_coupons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tendoo_nexo_coupons` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CODE` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `DESCRIPTION` text COLLATE utf8_unicode_ci NOT NULL,
  `DATE_CREATION` datetime NOT NULL,
  `DATE_MOD` datetime NOT NULL,
  `AUTHOR` int(11) NOT NULL,
  `DISCOUNT_TYPE` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `AMOUNT` float NOT NULL,
  `EXPIRY_DATE` datetime NOT NULL,
  `USAGE_COUNT` int(11) NOT NULL,
  `INDIVIDUAL_USE` int(11) NOT NULL,
  `PRODUCTS_IDS` text COLLATE utf8_unicode_ci NOT NULL,
  `EXCLUDE_PRODUCTS_IDS` text COLLATE utf8_unicode_ci NOT NULL,
  `USAGE_LIMIT` int(11) NOT NULL,
  `USAGE_LIMIT_PER_USER` int(11) NOT NULL,
  `LIMIT_USAGE_TO_X_ITEMS` int(11) NOT NULL,
  `FREE_SHIPPING` int(11) NOT NULL,
  `PRODUCT_CATEGORIES` text COLLATE utf8_unicode_ci NOT NULL,
  `EXCLUDE_PRODUCT_CATEGORIES` text COLLATE utf8_unicode_ci NOT NULL,
  `EXCLUDE_SALE_ITEMS` int(11) NOT NULL,
  `MINIMUM_AMOUNT` float NOT NULL,
  `MAXIMUM_AMOUNT` float NOT NULL,
  `USED_BY` text COLLATE utf8_unicode_ci NOT NULL,
  `REWARDED_CASHIER` int(11) NOT NULL,
  `EMAIL_RESTRICTIONS` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tendoo_nexo_coupons`
--

LOCK TABLES `tendoo_nexo_coupons` WRITE;
/*!40000 ALTER TABLE `tendoo_nexo_coupons` DISABLE KEYS */;
/*!40000 ALTER TABLE `tendoo_nexo_coupons` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tendoo_nexo_daily_log`
--

DROP TABLE IF EXISTS `tendoo_nexo_daily_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tendoo_nexo_daily_log` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `JSON` text COLLATE utf8_unicode_ci NOT NULL,
  `DATE_CREATION` datetime NOT NULL,
  `DATE_MOD` datetime NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tendoo_nexo_daily_log`
--

LOCK TABLES `tendoo_nexo_daily_log` WRITE;
/*!40000 ALTER TABLE `tendoo_nexo_daily_log` DISABLE KEYS */;
INSERT INTO `tendoo_nexo_daily_log` VALUES (1,'{\"unpaid_nbr\":0,\"partially_nbr\":0,\"paid_nbr\":0,\"total_discount\":0,\"total_taxes\":0,\"total_sales\":0,\"total_paid\":0,\"total_partially\":0,\"total_unpaid\":0,\"day_of_week\":0}','2018-12-22 23:59:59','0000-00-00 00:00:00'),(2,'{\"unpaid_nbr\":1,\"partially_nbr\":0,\"paid_nbr\":0,\"total_discount\":0,\"total_taxes\":0,\"total_sales\":123,\"total_paid\":0,\"total_partially\":0,\"total_unpaid\":123,\"day_of_week\":1}','2018-12-23 23:59:59','0000-00-00 00:00:00'),(3,'{\"unpaid_nbr\":11,\"partially_nbr\":0,\"paid_nbr\":0,\"total_discount\":0,\"total_taxes\":0,\"total_sales\":57405,\"total_paid\":0,\"total_partially\":0,\"total_unpaid\":57405,\"day_of_week\":2}','2018-12-24 23:59:59','0000-00-00 00:00:00'),(4,'{\"unpaid_nbr\":35,\"partially_nbr\":3,\"paid_nbr\":51,\"total_discount\":0,\"total_taxes\":0,\"total_sales\":10429.419999999998,\"total_paid\":3887.4,\"total_partially\":961,\"total_unpaid\":5581.02,\"day_of_week\":3}','2018-12-25 23:59:59','0000-00-00 00:00:00'),(5,'{\"unpaid_nbr\":0,\"partially_nbr\":0,\"paid_nbr\":0,\"total_discount\":0,\"total_taxes\":0,\"total_sales\":0,\"total_paid\":0,\"total_partially\":0,\"total_unpaid\":0,\"day_of_week\":4}','2018-12-26 23:59:59','0000-00-00 00:00:00'),(6,'{\"unpaid_nbr\":11,\"partially_nbr\":2,\"paid_nbr\":1,\"total_discount\":0,\"total_taxes\":0,\"total_sales\":252.86000000000004,\"total_paid\":18.99,\"total_partially\":35.98,\"total_unpaid\":197.89000000000001,\"day_of_week\":4}','2018-12-27 23:59:59','0000-00-00 00:00:00'),(7,'{\"unpaid_nbr\":0,\"partially_nbr\":0,\"paid_nbr\":0,\"total_discount\":0,\"total_taxes\":0,\"total_sales\":0,\"total_paid\":0,\"total_partially\":0,\"total_unpaid\":0,\"day_of_week\":5}','2019-01-18 23:59:59','0000-00-00 00:00:00'),(8,'{\"unpaid_nbr\":0,\"partially_nbr\":0,\"paid_nbr\":0,\"total_discount\":0,\"total_taxes\":0,\"total_sales\":0,\"total_paid\":0,\"total_partially\":0,\"total_unpaid\":0,\"day_of_week\":5}','2019-01-17 23:59:59','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `tendoo_nexo_daily_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tendoo_nexo_fournisseurs`
--

DROP TABLE IF EXISTS `tendoo_nexo_fournisseurs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tendoo_nexo_fournisseurs` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NOM` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `BP` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `TEL` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `EMAIL` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `DATE_CREATION` datetime NOT NULL,
  `DATE_MOD` datetime NOT NULL,
  `AUTHOR` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `DESCRIPTION` text COLLATE utf8_unicode_ci NOT NULL,
  `PAYABLE` float NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tendoo_nexo_fournisseurs`
--

LOCK TABLES `tendoo_nexo_fournisseurs` WRITE;
/*!40000 ALTER TABLE `tendoo_nexo_fournisseurs` DISABLE KEYS */;
INSERT INTO `tendoo_nexo_fournisseurs` VALUES (1,'Charlotte Russe','','','vendor@tendoo.org','2018-12-27 12:17:37','0000-00-00 00:00:00','2','',0);
/*!40000 ALTER TABLE `tendoo_nexo_fournisseurs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tendoo_nexo_fournisseurs_history`
--

DROP TABLE IF EXISTS `tendoo_nexo_fournisseurs_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tendoo_nexo_fournisseurs_history` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TYPE` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `BEFORE_AMOUNT` float NOT NULL,
  `AMOUNT` float NOT NULL,
  `AFTER_AMOUNT` float NOT NULL,
  `REF_PROVIDER` int(11) NOT NULL,
  `REF_INVOICE` int(11) NOT NULL,
  `REF_SUPPLY` int(11) NOT NULL,
  `DATE_CREATION` datetime NOT NULL,
  `DATE_MOD` datetime NOT NULL,
  `AUTHOR` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tendoo_nexo_fournisseurs_history`
--

LOCK TABLES `tendoo_nexo_fournisseurs_history` WRITE;
/*!40000 ALTER TABLE `tendoo_nexo_fournisseurs_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `tendoo_nexo_fournisseurs_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tendoo_nexo_historique`
--

DROP TABLE IF EXISTS `tendoo_nexo_historique`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tendoo_nexo_historique` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TITRE` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `DETAILS` text COLLATE utf8_unicode_ci NOT NULL,
  `DATE_CREATION` datetime NOT NULL,
  `DATE_MOD` datetime NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tendoo_nexo_historique`
--

LOCK TABLES `tendoo_nexo_historique` WRITE;
/*!40000 ALTER TABLE `tendoo_nexo_historique` DISABLE KEYS */;
INSERT INTO `tendoo_nexo_historique` VALUES (1,'Create an order','A new <strong>5KI52A </strong> order with a total <strong>17.99 </strong> was created by <strong>admin </strong>.','2018-12-27 13:19:26','0000-00-00 00:00:00'),(2,'Create an order','A new <strong>DP8H9E </strong> order with a total <strong>17.99 </strong> was created by <strong>admin </strong>.','2018-12-27 14:02:25','0000-00-00 00:00:00'),(3,'Create an order','A new <strong>GFGQVL </strong> order with a total <strong>17.99 </strong> was created by <strong>admin </strong>.','2018-12-27 16:09:21','0000-00-00 00:00:00'),(4,'Create an order','A new <strong>VTKPU3 </strong> order with a total <strong>18.99 </strong> was created by <strong>admin </strong>.','2018-12-27 16:11:04','0000-00-00 00:00:00'),(5,'Create an order','A new <strong>AAXD5Z </strong> order with a total <strong>17.99 </strong> was created by <strong>admin </strong>.','2018-12-27 16:16:25','0000-00-00 00:00:00'),(6,'Create an order','A new <strong>KNNUK5 </strong> order with a total <strong>17.99 </strong> was created by <strong>admin </strong>.','2018-12-27 16:18:12','0000-00-00 00:00:00'),(7,'Create an order','A new <strong>FT6OOL </strong> order with a total <strong>17.99 </strong> was created by <strong>admin </strong>.','2018-12-27 16:19:54','0000-00-00 00:00:00'),(8,'Create an order','A new <strong>13UYAC </strong> order with a total <strong>17.99 </strong> was created by <strong>admin </strong>.','2018-12-27 16:22:56','0000-00-00 00:00:00'),(9,'Create an order','A new <strong>N2Q8I3 </strong> order with a total <strong>17.99 </strong> was created by <strong>admin </strong>.','2018-12-27 16:26:18','0000-00-00 00:00:00'),(10,'Create an order','A new <strong>94JWMI </strong> order with a total <strong>17.99 </strong> was created by <strong>admin </strong>.','2018-12-27 16:27:26','0000-00-00 00:00:00'),(11,'Create an order','A new <strong>TI6VZW </strong> order with a total <strong>17.99 </strong> was created by <strong>admin </strong>.','2018-12-27 16:27:54','0000-00-00 00:00:00'),(12,'Create an order','A new <strong>IAN7ZU </strong> order with a total <strong>17.99 </strong> was created by <strong>admin </strong>.','2018-12-27 16:30:22','0000-00-00 00:00:00'),(13,'Create an order','A new <strong>FQA7X3 </strong> order with a total <strong>17.99 </strong> was created by <strong>admin </strong>.','2018-12-27 16:33:49','0000-00-00 00:00:00'),(14,'Create an order','A new <strong>YSXQBO </strong> order with a total <strong>17.99 </strong> was created by <strong>admin </strong>.','2018-12-27 16:36:33','0000-00-00 00:00:00'),(15,'Order Deleted','The <strong>GFGQVL </strong> order with a total <strong>17.99 </strong> has been removed by the user <strong>admin </strong>','2019-01-18 14:44:00','0000-00-00 00:00:00'),(16,'Deleting an order','Useradmin removed an order with the following id:3','2019-01-18 14:44:00','0000-00-00 00:00:00'),(17,'Order Deleted','The <strong>AAXD5Z </strong> order with a total <strong>17.99 </strong> has been removed by the user <strong>admin </strong>','2019-01-18 14:44:00','0000-00-00 00:00:00'),(18,'Deleting an order','Useradmin removed an order with the following id:5','2019-01-18 14:44:00','0000-00-00 00:00:00'),(19,'Order Deleted','The <strong>KNNUK5 </strong> order with a total <strong>17.99 </strong> has been removed by the user <strong>admin </strong>','2019-01-18 14:44:00','0000-00-00 00:00:00'),(20,'Deleting an order','Useradmin removed an order with the following id:6','2019-01-18 14:44:00','0000-00-00 00:00:00'),(21,'Order Deleted','The <strong>FT6OOL </strong> order with a total <strong>17.99 </strong> has been removed by the user <strong>admin </strong>','2019-01-18 14:44:00','0000-00-00 00:00:00'),(22,'Deleting an order','Useradmin removed an order with the following id:7','2019-01-18 14:44:00','0000-00-00 00:00:00'),(23,'Order Deleted','The <strong>13UYAC </strong> order with a total <strong>17.99 </strong> has been removed by the user <strong>admin </strong>','2019-01-18 14:44:00','0000-00-00 00:00:00'),(24,'Deleting an order','Useradmin removed an order with the following id:8','2019-01-18 14:44:00','0000-00-00 00:00:00'),(25,'Order Deleted','The <strong>N2Q8I3 </strong> order with a total <strong>17.99 </strong> has been removed by the user <strong>admin </strong>','2019-01-18 14:44:00','0000-00-00 00:00:00'),(26,'Deleting an order','Useradmin removed an order with the following id:9','2019-01-18 14:44:00','0000-00-00 00:00:00'),(27,'Order Deleted','The <strong>94JWMI </strong> order with a total <strong>17.99 </strong> has been removed by the user <strong>admin </strong>','2019-01-18 14:44:00','0000-00-00 00:00:00'),(28,'Deleting an order','Useradmin removed an order with the following id:10','2019-01-18 14:44:00','0000-00-00 00:00:00'),(29,'Order Deleted','The <strong>TI6VZW </strong> order with a total <strong>17.99 </strong> has been removed by the user <strong>admin </strong>','2019-01-18 14:44:00','0000-00-00 00:00:00'),(30,'Deleting an order','Useradmin removed an order with the following id:11','2019-01-18 14:44:00','0000-00-00 00:00:00'),(31,'Order Deleted','The <strong>IAN7ZU </strong> order with a total <strong>17.99 </strong> has been removed by the user <strong>admin </strong>','2019-01-18 14:44:00','0000-00-00 00:00:00'),(32,'Deleting an order','Useradmin removed an order with the following id:12','2019-01-18 14:44:00','0000-00-00 00:00:00'),(33,'Order Deleted','The <strong>FQA7X3 </strong> order with a total <strong>17.99 </strong> has been removed by the user <strong>admin </strong>','2019-01-18 14:44:00','0000-00-00 00:00:00'),(34,'Deleting an order','Useradmin removed an order with the following id:13','2019-01-18 14:44:00','0000-00-00 00:00:00'),(35,'Order Deleted','The <strong>YSXQBO </strong> order with a total <strong>17.99 </strong> has been removed by the user <strong>admin </strong>','2019-01-18 14:44:00','0000-00-00 00:00:00'),(36,'Deleting an order','Useradmin removed an order with the following id:14','2019-01-18 14:44:00','0000-00-00 00:00:00'),(37,'Automatic removal of the quote orders','The following order has been deleted automatically for expiration: <ul><li>GFGQVL</li><li>AAXD5Z</li><li>KNNUK5</li><li>FT6OOL</li><li>13UYAC</li><li>N2Q8I3</li><li>94JWMI</li><li>TI6VZW</li><li>IAN7ZU</li><li>FQA7X3</li><li>YSXQBO</li></ul> products have been restored in the shop.','2019-01-18 14:44:00','0000-00-00 00:00:00'),(38,'Create an order','A new <strong>YAX074 </strong> order with a total <strong>17.99 </strong> was created by <strong>admin </strong>.','2019-01-18 14:45:01','0000-00-00 00:00:00'),(39,'Create an order','A new <strong>AS7J0F </strong> order with a total <strong>17.99 </strong> was created by <strong>admin </strong>.','2019-01-18 14:45:31','0000-00-00 00:00:00'),(40,'Order Deleted','The <strong>AS7J0F </strong> order with a total <strong>17.99 </strong> has been removed by the user <strong>admin </strong>','2019-01-18 14:48:32','0000-00-00 00:00:00'),(41,'Deleting an order','Useradmin removed an order with the following id:16','2019-01-18 14:48:32','0000-00-00 00:00:00'),(42,'Order Deleted','The <strong>YAX074 </strong> order with a total <strong>17.99 </strong> has been removed by the user <strong>admin </strong>','2019-01-18 14:48:32','0000-00-00 00:00:00'),(43,'Deleting an order','Useradmin removed an order with the following id:15','2019-01-18 14:48:32','0000-00-00 00:00:00'),(44,'Order Deleted','The <strong>VTKPU3 </strong> order with a total <strong>18.99 </strong> has been removed by the user <strong>admin </strong>','2019-01-18 14:48:32','0000-00-00 00:00:00'),(45,'Deleting an order','Useradmin removed an order with the following id:4','2019-01-18 14:48:32','0000-00-00 00:00:00'),(46,'Order Deleted','The <strong>DP8H9E </strong> order with a total <strong>17.99 </strong> has been removed by the user <strong>admin </strong>','2019-01-18 14:48:32','0000-00-00 00:00:00'),(47,'Deleting an order','Useradmin removed an order with the following id:2','2019-01-18 14:48:32','0000-00-00 00:00:00'),(48,'Order Deleted','The <strong>5KI52A </strong> order with a total <strong>17.99 </strong> has been removed by the user <strong>admin </strong>','2019-01-18 14:48:32','0000-00-00 00:00:00'),(49,'Deleting an order','Useradmin removed an order with the following id:1','2019-01-18 14:48:32','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `tendoo_nexo_historique` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tendoo_nexo_notices`
--

DROP TABLE IF EXISTS `tendoo_nexo_notices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tendoo_nexo_notices` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TYPE` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `TITLE` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `MESSAGE` text COLLATE utf8_unicode_ci NOT NULL,
  `ICON` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `LINK` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `REF_USER` int(11) NOT NULL,
  `DATE_CREATION` datetime NOT NULL,
  `DATE_MOD` datetime NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tendoo_nexo_notices`
--

LOCK TABLES `tendoo_nexo_notices` WRITE;
/*!40000 ALTER TABLE `tendoo_nexo_notices` DISABLE KEYS */;
/*!40000 ALTER TABLE `tendoo_nexo_notices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tendoo_nexo_premium_backups`
--

DROP TABLE IF EXISTS `tendoo_nexo_premium_backups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tendoo_nexo_premium_backups` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `FILE_LOCATION` text COLLATE utf8_unicode_ci NOT NULL,
  `DATE_CREATION` datetime NOT NULL,
  `DATE_MODIFICATION` datetime NOT NULL,
  `AUTHOR` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tendoo_nexo_premium_backups`
--

LOCK TABLES `tendoo_nexo_premium_backups` WRITE;
/*!40000 ALTER TABLE `tendoo_nexo_premium_backups` DISABLE KEYS */;
/*!40000 ALTER TABLE `tendoo_nexo_premium_backups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tendoo_nexo_premium_factures`
--

DROP TABLE IF EXISTS `tendoo_nexo_premium_factures`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tendoo_nexo_premium_factures` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `INTITULE` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `REF` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `MONTANT` float NOT NULL,
  `REF_CATEGORY` int(11) NOT NULL,
  `REF_PROVIDER` int(11) NOT NULL,
  `REF_USER` int(11) NOT NULL,
  `IMAGE` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `DESCRIPTION` text COLLATE utf8_unicode_ci NOT NULL,
  `DATE_CREATION` datetime NOT NULL,
  `DATE_MODIFICATION` datetime NOT NULL,
  `AUTHOR` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tendoo_nexo_premium_factures`
--

LOCK TABLES `tendoo_nexo_premium_factures` WRITE;
/*!40000 ALTER TABLE `tendoo_nexo_premium_factures` DISABLE KEYS */;
/*!40000 ALTER TABLE `tendoo_nexo_premium_factures` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tendoo_nexo_premium_factures_categories`
--

DROP TABLE IF EXISTS `tendoo_nexo_premium_factures_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tendoo_nexo_premium_factures_categories` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `DESCRIPTION` text COLLATE utf8_unicode_ci NOT NULL,
  `DATE_CREATION` datetime NOT NULL,
  `DATE_MODIFICATION` datetime NOT NULL,
  `AUTHOR` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tendoo_nexo_premium_factures_categories`
--

LOCK TABLES `tendoo_nexo_premium_factures_categories` WRITE;
/*!40000 ALTER TABLE `tendoo_nexo_premium_factures_categories` DISABLE KEYS */;
/*!40000 ALTER TABLE `tendoo_nexo_premium_factures_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tendoo_nexo_premium_factures_items`
--

DROP TABLE IF EXISTS `tendoo_nexo_premium_factures_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tendoo_nexo_premium_factures_items` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `PRICE` float NOT NULL,
  `QUANTITY` float NOT NULL,
  `TOTAL` float NOT NULL,
  `FLAT_DISCOUNT` float NOT NULL,
  `PERCENTAGE_DISCOUNT` float NOT NULL,
  `DISCOUNT_TYPE` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `REF_INVOICE` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tendoo_nexo_premium_factures_items`
--

LOCK TABLES `tendoo_nexo_premium_factures_items` WRITE;
/*!40000 ALTER TABLE `tendoo_nexo_premium_factures_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `tendoo_nexo_premium_factures_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tendoo_nexo_rayons`
--

DROP TABLE IF EXISTS `tendoo_nexo_rayons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tendoo_nexo_rayons` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TITRE` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `DESCRIPTION` text COLLATE utf8_unicode_ci NOT NULL,
  `DATE_CREATION` datetime NOT NULL,
  `DATE_MOD` datetime NOT NULL,
  `AUTHOR` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tendoo_nexo_rayons`
--

LOCK TABLES `tendoo_nexo_rayons` WRITE;
/*!40000 ALTER TABLE `tendoo_nexo_rayons` DISABLE KEYS */;
INSERT INTO `tendoo_nexo_rayons` VALUES (1,'Men','Men Radius','2018-12-27 12:17:37','0000-00-00 00:00:00',2),(2,'Womens','Ray of Women','2018-12-27 12:17:37','0000-00-00 00:00:00',2),(3,'Children','Ray of children','2018-12-27 12:17:37','0000-00-00 00:00:00',2),(4,'Gifts','Gifts rays','2018-12-27 12:17:37','0000-00-00 00:00:00',2);
/*!40000 ALTER TABLE `tendoo_nexo_rayons` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tendoo_nexo_registers`
--

DROP TABLE IF EXISTS `tendoo_nexo_registers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tendoo_nexo_registers` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `DESCRIPTION` text COLLATE utf8_unicode_ci NOT NULL,
  `IMAGE_URL` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `NPS_URL` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `ASSIGNED_PRINTER` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `AUTHOR` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `DATE_CREATION` datetime NOT NULL,
  `DATE_MOD` datetime NOT NULL,
  `STATUS` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `USED_BY` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tendoo_nexo_registers`
--

LOCK TABLES `tendoo_nexo_registers` WRITE;
/*!40000 ALTER TABLE `tendoo_nexo_registers` DISABLE KEYS */;
INSERT INTO `tendoo_nexo_registers` VALUES (1,'Checkout 1','',NULL,'','','2','2018-12-27 12:17:37','0000-00-00 00:00:00','closed',0),(2,'Checkout 2','',NULL,'','','2','2018-12-27 12:17:37','0000-00-00 00:00:00','closed',0),(3,'Checkout 3','',NULL,'','','2','2018-12-27 12:17:37','0000-00-00 00:00:00','locked',0);
/*!40000 ALTER TABLE `tendoo_nexo_registers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tendoo_nexo_registers_activities`
--

DROP TABLE IF EXISTS `tendoo_nexo_registers_activities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tendoo_nexo_registers_activities` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `AUTHOR` int(11) NOT NULL,
  `TYPE` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `BALANCE` float NOT NULL,
  `DATE_CREATION` datetime NOT NULL,
  `DATE_MOD` datetime NOT NULL,
  `NOTE` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `REF_REGISTER` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tendoo_nexo_registers_activities`
--

LOCK TABLES `tendoo_nexo_registers_activities` WRITE;
/*!40000 ALTER TABLE `tendoo_nexo_registers_activities` DISABLE KEYS */;
/*!40000 ALTER TABLE `tendoo_nexo_registers_activities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tendoo_nexo_stores`
--

DROP TABLE IF EXISTS `tendoo_nexo_stores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tendoo_nexo_stores` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `AUTHOR` int(11) NOT NULL,
  `STATUS` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `IMAGE` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `DESCRIPTION` text COLLATE utf8_unicode_ci NOT NULL,
  `DATE_CREATION` datetime NOT NULL,
  `DATE_MOD` datetime NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tendoo_nexo_stores`
--

LOCK TABLES `tendoo_nexo_stores` WRITE;
/*!40000 ALTER TABLE `tendoo_nexo_stores` DISABLE KEYS */;
/*!40000 ALTER TABLE `tendoo_nexo_stores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tendoo_nexo_stores_activities`
--

DROP TABLE IF EXISTS `tendoo_nexo_stores_activities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tendoo_nexo_stores_activities` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `AUTHOR` int(11) NOT NULL,
  `TYPE` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `REF_STORE` int(11) NOT NULL,
  `DATE_CREATION` datetime NOT NULL,
  `DATE_MOD` datetime NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tendoo_nexo_stores_activities`
--

LOCK TABLES `tendoo_nexo_stores_activities` WRITE;
/*!40000 ALTER TABLE `tendoo_nexo_stores_activities` DISABLE KEYS */;
/*!40000 ALTER TABLE `tendoo_nexo_stores_activities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tendoo_nexo_taxes`
--

DROP TABLE IF EXISTS `tendoo_nexo_taxes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tendoo_nexo_taxes` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `DESCRIPTION` text COLLATE utf8_unicode_ci NOT NULL,
  `RATE` float NOT NULL,
  `TYPE` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `FIXED` float NOT NULL,
  `AUTHOR` int(11) NOT NULL,
  `DATE_CREATION` datetime NOT NULL,
  `DATE_MOD` datetime NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tendoo_nexo_taxes`
--

LOCK TABLES `tendoo_nexo_taxes` WRITE;
/*!40000 ALTER TABLE `tendoo_nexo_taxes` DISABLE KEYS */;
/*!40000 ALTER TABLE `tendoo_nexo_taxes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tendoo_nexo_users_activities`
--

DROP TABLE IF EXISTS `tendoo_nexo_users_activities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tendoo_nexo_users_activities` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `AUTHOR` int(11) NOT NULL,
  `MESSAGE` text COLLATE utf8_unicode_ci NOT NULL,
  `DATE_CREATION` datetime NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tendoo_nexo_users_activities`
--

LOCK TABLES `tendoo_nexo_users_activities` WRITE;
/*!40000 ALTER TABLE `tendoo_nexo_users_activities` DISABLE KEYS */;
/*!40000 ALTER TABLE `tendoo_nexo_users_activities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tendoo_options`
--

DROP TABLE IF EXISTS `tendoo_options`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tendoo_options` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `autoload` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `app` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=106 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tendoo_options`
--

LOCK TABLES `tendoo_options` WRITE;
/*!40000 ALTER TABLE `tendoo_options` DISABLE KEYS */;
INSERT INTO `tendoo_options` VALUES (1,'rest_key','3QjkaFh6iOxZdWwo1J3E87sxUMdQ6JDWMJy30tpA',1,0,'system'),(2,'actives_modules','{\"0\":\"nexo_premium\",\"1\":\"grocerycrud\",\"2\":\"nexo\",\"3\":\"nexo_sms\",\"5\":\"perm_manager\",\"7\":\"nexo-globalonepay-gateway\",\"8\":\"nexo-updater\",\"9\":\"nexo-payments-gateway\"}',1,0,'system'),(3,'nexo_premium_installed','true',1,0,'system'),(4,'nexo_sms_invoice_template','{{ site_name }} \\n \n        Bonjour {{ name }}, votre commande {{ order_code }} est prête. \\n\n        Total {{ order_topay }} \\n \n        Merci pour votre confiance',1,0,'system'),(5,'database_version','1.2',1,0,'system'),(6,'site_name','NexoPOS',1,0,'system'),(7,'site_language','en_US',1,0,'system'),(8,'nexo_installed','true',1,0,'system'),(9,'nexo_display_select_client','enable',1,0,'system'),(10,'nexo_display_payment_means','enable',1,0,'system'),(11,'nexo_display_amount_received','enable',1,0,'system'),(12,'nexo_display_discount','enable',1,0,'system'),(13,'nexo_currency_position','before',1,0,'system'),(14,'nexo_receipt_theme','light',1,0,'system'),(15,'nexo_enable_autoprinting','no',1,0,'system'),(16,'nexo_devis_expiration','7',1,0,'system'),(17,'nexo_shop_street','Cameroon, Yaoundé Ngousso Av.',1,0,'system'),(18,'nexo_shop_pobox','45 Edéa Cameroon',1,0,'system'),(19,'nexo_shop_email','carlosjohnsonluv2004@gmail.com',1,0,'system'),(20,'how_many_before_discount','',1,0,'system'),(21,'nexo_products_labels','5',1,0,'system'),(22,'nexo_codebar_height','100',1,0,'system'),(23,'nexo_bar_width','3',1,0,'system'),(24,'nexo_soundfx','enable',1,0,'system'),(25,'nexo_currency','$',1,0,'system'),(26,'nexo_vat_percent','10',1,0,'system'),(27,'nexo_enable_autoprint','yes',1,0,'system'),(28,'nexo_enable_smsinvoice','no',1,0,'system'),(29,'nexo_currency_iso','USD',1,0,'system'),(30,'nexo_compact_enabled','yes',1,0,'system'),(31,'nexo_enable_shadow_price','no',1,0,'system'),(32,'nexo_enable_stripe','yes',1,0,'system'),(33,'user_id','2',1,0,'system'),(34,'nexo_first_run','true',1,0,'system'),(35,'daily-log','2019-01-17 23:59:59',1,0,'system'),(36,'first-name','manager',1,3,'users'),(37,'last-name','manager',1,3,'users'),(38,'theme-skin','skin-blue',1,3,'users'),(39,'first-name','test',1,4,'users'),(40,'last-name','test',1,4,'users'),(41,'theme-skin','skin-blue',1,4,'users'),(42,'nexo_enable_globalonepay','yes',1,0,'system'),(43,'nexo_globalonepay_test_mode','yes',1,0,'system'),(44,'nexo_globalonepay_terminal_id','33001',1,0,'system'),(45,'nexo_globalonepay_shared_secret','SandboxSecret001',1,0,'system'),(46,'enable_group_discount','disable',1,0,'system'),(47,'discount_type','disable',1,0,'system'),(48,'discount_percent','',1,0,'system'),(49,'discount_amount','',1,0,'system'),(50,'default_compte_client','1',1,0,'system'),(51,'migration_nexo-updater',NULL,1,0,'system'),(52,'nexo_updater_modules_status','{\"status\":\"failed\",\"message\":\"Unable to find any licence attached to your domain. Please consider registering this domain with a licence\"}',1,0,'system'),(53,'update_menu_notices','0',1,0,'system'),(54,'nexopos_store_access_key','PIVHx7FmqQIATv0XwpJ2D97eu7w2oWT5mVcZBXqP',1,0,'system'),(55,'updater2_validated','yes',1,0,'system'),(56,'order_code','{\"0\":\"QTDNBY\",\"1\":\"JRUY5K\",\"2\":\"03G4SK\",\"3\":\"L4HWDP\",\"4\":\"ABF2EA\",\"5\":\"OFKAQG\",\"6\":\"UWOB01\",\"7\":\"V3AD58\",\"8\":\"7T8AKP\",\"9\":\"P3H9PC\",\"10\":\"WLHBDL\",\"11\":\"E36136\",\"12\":\"VMIMXW\",\"13\":\"XWVH24\",\"14\":\"RDYFGD\",\"15\":\"TL5WI5\",\"16\":\"XBS563\",\"17\":\"M1H78Q\",\"18\":\"RJ496U\",\"19\":\"8OOLSW\",\"20\":\"5XCW0C\",\"21\":\"IGWAKO\",\"22\":\"O8WGS4\",\"23\":\"U1WAQK\",\"24\":\"PYAOWU\",\"25\":\"I6WKYK\",\"26\":\"N8B24U\",\"27\":\"GB3I7Z\",\"28\":\"9N2PGZ\",\"29\":\"7RL731\",\"30\":\"47YQG8\",\"31\":\"H2GOFN\",\"32\":\"6KA7UA\",\"33\":\"T5PPOP\",\"34\":\"60IMJ2\",\"35\":\"5IGB8P\",\"36\":\"AN3AOF\",\"37\":\"V2QSTV\",\"38\":\"8GRBIP\",\"39\":\"P0VUMV\",\"40\":\"U9645N\",\"41\":\"43MBHA\",\"42\":\"I0PFZX\",\"43\":\"XB5ZWA\",\"44\":\"NB3OW3\",\"45\":\"WHZB6J\",\"46\":\"4OSDMN\",\"47\":\"T7QT3D\",\"49\":\"ABXV00\",\"50\":\"S46NBH\",\"51\":\"OMVP4P\",\"53\":\"TOTCUR\",\"54\":\"93QK93\",\"55\":\"YS7N8C\",\"56\":\"OOVIG1\",\"57\":\"HFNRFA\",\"58\":\"HRDQ5S\",\"59\":\"5KW9DB\",\"60\":\"PD6MAI\",\"61\":\"BEK5LO\",\"62\":\"92W4UY\",\"63\":\"JOMMD2\",\"64\":\"WQZNOF\",\"65\":\"IY4GQ7\",\"66\":\"SE9SIL\",\"67\":\"23SW8T\",\"68\":\"LC70A9\",\"70\":\"UYD3F2\",\"71\":\"UO0XQL\",\"72\":\"T5KWFH\",\"74\":\"PP4E81\",\"75\":\"6945J2\",\"76\":\"PG5OOO\",\"77\":\"LQ9NQM\",\"78\":\"X43A7T\",\"79\":\"0L8OTA\",\"80\":\"GTPYQR\",\"81\":\"4IRBY3\",\"82\":\"A2RMP9\",\"83\":\"TLI5FY\",\"84\":\"JXXH33\",\"85\":\"XQ96G9\",\"86\":\"NRNCQD\",\"87\":\"RUD4XM\",\"88\":\"FVB54F\",\"89\":\"IMWFDH\",\"90\":\"1PWNH4\",\"91\":\"PASTBR\",\"92\":\"KP5KBT\",\"93\":\"ENB843\",\"94\":\"MQU46F\",\"95\":\"AGJH0J\",\"96\":\"D7TST4\",\"97\":\"76Y3JV\",\"98\":\"DAZMVW\",\"99\":\"JL47XC\",\"100\":\"Z0WFCN\",\"101\":\"1FZRV8\",\"102\":\"2YUOPI\",\"103\":\"P76EJY\",\"104\":\"C472GL\",\"105\":\"UYVAHG\",\"106\":\"EAZR86\",\"107\":\"JL7IYW\",\"108\":\"HBAIY2\",\"109\":\"XMPBX0\",\"110\":\"ZLQUDC\",\"111\":\"2ZCVGH\",\"112\":\"YROUG9\",\"113\":\"Q3JKJD\",\"114\":\"G1IFJN\",\"115\":\"ZB2YHD\",\"116\":\"2UVF4R\",\"117\":\"SN95PN\",\"119\":\"TU1VY8\",\"120\":\"YGB8PA\",\"121\":\"UNMSVR\",\"122\":\"4HGI5D\",\"124\":\"DP8H9E\",\"126\":\"VTKPU3\",\"127\":\"YAX074\",\"128\":\"AS7J0F\"}',0,0,'system'),(57,'site_registration','0',1,0,'system'),(58,'require_validation','0',1,0,'system'),(59,'webdev_mode','0',1,0,'system'),(60,'migration_nexo-globalonepay-gateway',NULL,1,0,'system'),(61,'nexo_stripe_publishable_key','pk_test_btbkkkWMdIGFXWAFYJvLM3mP',1,0,'system'),(62,'nexo_stripe_secret_key','sk_test_SQzczHWkwNQnFdItvqmJL49H',1,0,'system'),(63,'nexo_shop_address_1','',1,0,'system'),(64,'nexo_shop_address_2','',1,0,'system'),(65,'nexo_shop_city','',1,0,'system'),(66,'nexo_shop_phone','+237656988008',1,0,'system'),(67,'nexo_shop_fax','',1,0,'system'),(68,'nexo_other_details','',1,0,'system'),(69,'nexo_disable_frontend','enable',1,0,'system'),(70,'nexo_enable_christmas_effect','disable',1,0,'system'),(71,'nexo_premium_enable_history','yes',1,0,'system'),(72,'nexo_globalonepay_endpoint','https://testpayments.globalone.me/merchant/xmlpayment',1,0,'system'),(73,'nexo_logo_type','disable',1,0,'system'),(74,'nexo_logo_text','nexoPOS',1,0,'system'),(75,'nexo_logo_url','',1,0,'system'),(76,'nexo_logo_width','180',1,0,'system'),(77,'nexo_logo_height','34',1,0,'system'),(78,'nexo_footer_text','',1,0,'system'),(79,'nexo_date_format','Y-m-d',1,0,'system'),(80,'nexo_datetime_format','Y-m-d H:i',1,0,'system'),(81,'nexo_js_datetime_format','',1,0,'system'),(82,'nexo_print_gateway','normal_print',1,0,'system'),(83,'nexo_print_server_url','',1,0,'system'),(84,'nexo_pos_printer','',1,0,'system'),(85,'unit_price_changing','yes',1,0,'system'),(86,'show_item_taxes','no',1,0,'system'),(87,'enable_quick_search','no',1,0,'system'),(88,'auto_submit_barcode_entry','',1,0,'system'),(89,'nexo_enable_numpad','',1,0,'system'),(90,'keyshortcuts','',1,0,'system'),(91,'disable_partial_order','',1,0,'system'),(92,'nexo_enable_registers','non',1,0,'system'),(93,'nexo_vat_type','disabled',1,0,'system'),(94,'unit_item_discount_enabled','no',1,0,'system'),(95,'hide_discount_button','no',1,0,'system'),(96,'disable_coupon','no',1,0,'system'),(97,'disable_shipping','no',1,0,'system'),(98,'disable_customer_creation','no',1,0,'system'),(99,'disable_quick_item','no',1,0,'system'),(100,'nexo_sms_service','disable',1,0,'system'),(101,'receipt_col_1','customer name : {customer_name}\n        order:  {order_code}',1,0,'system'),(102,'receipt_col_2','Cashier : {order_cashier}',1,0,'system'),(103,'nexo_store','disabled',1,0,'system'),(104,'nexo_enable_sound','enable',0,0,'system'),(105,'nexo_globalonepay_logs','yes',1,0,'system');
/*!40000 ALTER TABLE `tendoo_options` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tendoo_restapi_keys`
--

DROP TABLE IF EXISTS `tendoo_restapi_keys`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tendoo_restapi_keys` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(40) NOT NULL,
  `scopes` text DEFAULT NULL,
  `app_name` varchar(40) NOT NULL,
  `level` int(2) NOT NULL,
  `ignore_limits` tinyint(1) NOT NULL DEFAULT 0,
  `user` int(11) NOT NULL,
  `date_created` datetime NOT NULL,
  `expire` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tendoo_restapi_keys`
--

LOCK TABLES `tendoo_restapi_keys` WRITE;
/*!40000 ALTER TABLE `tendoo_restapi_keys` DISABLE KEYS */;
INSERT INTO `tendoo_restapi_keys` VALUES (1,'3QjkaFh6iOxZdWwo1J3E87sxUMdQ6JDWMJy30tpA','core','Tendoo CMS',0,0,0,'2018-12-23 17:50:55','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `tendoo_restapi_keys` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tendoo_system_sessions`
--

DROP TABLE IF EXISTS `tendoo_system_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tendoo_system_sessions` (
  `id` varchar(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `ip_address` varchar(45) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `timestamp` int(10) unsigned NOT NULL DEFAULT 0,
  `data` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tendoo_system_sessions`
--

LOCK TABLES `tendoo_system_sessions` WRITE;
/*!40000 ALTER TABLE `tendoo_system_sessions` DISABLE KEYS */;
/*!40000 ALTER TABLE `tendoo_system_sessions` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-01-18 17:55:16
